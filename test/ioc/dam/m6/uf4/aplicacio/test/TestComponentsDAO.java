package ioc.dam.m6.uf4.aplicacio.test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import ioc.dam.m6.persistencia.excepcions.UtilitatPersistenciaException;
import ioc.dam.m6.uf4.eac5.aplicacio.AplicacioBD;
import ioc.dam.m6.uf4.eac5.aplicacio.Municipi;
import ioc.dam.m6.uf4.eac5.aplicacio.Comarca;
import ioc.dam.m6.uf4.eac5.aplicacio.dao.AplicacioBDImpl;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author Professor
 * 
 */
public class TestComponentsDAO {
    
    private final  AplicacioBD aplicBD=new AplicacioBDImpl();

    
    
    private final ArrayList totesAltes=new ArrayList();
    private final ArrayList<Comarca> totesComarques = new ArrayList<>(),
            
    // Resultat de consultar les comarques despres de fer diferents proves
                                    totesComarquesExcepteEl5 = new ArrayList<>(),  // despres d'eliminar la comarca amb codi 5
                                    totesComarquesCanviatEl3 = new ArrayList<>(),  // despres de modificar la comarca amb codi 3
                                    comarquesDeSuperficie545=new ArrayList<>(),         // resultat de consultar les comarques de superficie 545
                                    comarquesDeSuperficie1002=new ArrayList<>();      // resultat de consultar les comarques de superficie 1002
    
    private final ArrayList<Municipi> totsMunicipis = new ArrayList<>(),
    // Resultat de consultar els municipis despres de fer diferents proves
                                    totesMunicipisExcepteEl2 = new ArrayList<>(),  // despres d'eliminar el municipi amb referència 22
                                    totesMunicipisCanviatEl3 = new ArrayList<>(),  // despres de modificar el municipi amb referència 33
                                    municipisDe110 = new ArrayList<>(),            // municipis de densitat 110.09
                                    municipisDe189 =new ArrayList<>(),           // municipis de densitat 189.61
                                    municipisDeValls=new ArrayList<>(),          // resultat de consultar els municipis que petenyen a la comarca de capital Valls
                                    municipisDeSabadellTerrassa=new ArrayList<>();   // resultat de consultar els municipis que petenyen a la comarca de capital Sabadell i Terrassa
private final ArrayList<Municipi> prova = new ArrayList<>();
    
    
    
    
    
    

    /**
     * Abans de cada test neteja la base de dades
     * @throws ioc.dam.m6.persistencia.excepcions.UtilitatPersistenciaException en cas d'error
     */
    @Before
    public void setUp() throws UtilitatPersistenciaException  {
        obre();
        creaDadesPrincipals();
        
        List<Municipi> auxLlistaMunicipis=aplicBD.obtenirMunicipis();
         
        List<Comarca> auxLlistaComarques=aplicBD.obtenirComarques();

          
        for(Municipi a:auxLlistaMunicipis){
            aplicBD.eliminar(a);
        }
        for(Comarca d:auxLlistaComarques){
            aplicBD.eliminar(d);
        }
        
        fesAltes();
        
    } 
    
    /**
     * En acabar els tests, tanca la connexio amb la base de dades
     */
    
    
    @After()
    public  void classEnds(){
        tanca();
    }
    
    @Test 
    public void provaAltes(){
        //es fan als tractaments previs
    }
    
    @Test
    public void provaConsultaTotesComarques() throws UtilitatPersistenciaException{
        assertTrue(comparaLlistesComarques(aplicBD.obtenirComarques(),totesComarques));
    }
    
    
    @Test
    public void provaConsultaTotsMunicipis() throws UtilitatPersistenciaException{
        assertTrue(comparaLlistesMunicipis(aplicBD.obtenirMunicipis(),totsMunicipis));        
    }
    
    @Test
    public void obtenirMunicipisPerDensitat() throws UtilitatPersistenciaException{
        assertTrue(comparaLlistesMunicipis(aplicBD.obtenirMunicipisPerDensitat((float) 110.09F),this.municipisDe110));   
        assertTrue(comparaLlistesMunicipis(aplicBD.obtenirMunicipisPerDensitat((float) 189.61F),this.municipisDe189)); 
    }
    
    @Test
    public void provaConsultarMunicipisPerCapitalComarca() throws UtilitatPersistenciaException{
        assertTrue(comparaLlistesMunicipis(aplicBD.obtenirMunicipisPerCapitalComarca("Sabadell i Terrassa"),this.municipisDeSabadellTerrassa));   
        assertTrue(comparaLlistesMunicipis(aplicBD.obtenirMunicipisPerCapitalComarca("Valls"),this.municipisDeValls));       
    }
    /*
    @Test
    public void provaConsultarComarquesPerSuperficie() throws UtilitatPersistenciaException{
        assertTrue(comparaLlistesComarques(aplicBD.obtenirComarquesPerSuperficie(1002),this.comarquesDeSuperficie1002));   
        assertTrue(comparaLlistesComarques(aplicBD.obtenirComarquesPerSuperficie(545),this.comarquesDeSuperficie545));  
    }
    */
    @Test
    public void provaEliminarMunicipi22() throws UtilitatPersistenciaException{
        Municipi a=new Municipi();
        a.setId_municipi(22);
        aplicBD.eliminar(a);
        assertTrue(comparaLlistesMunicipis(aplicBD.obtenirMunicipis(),totesMunicipisExcepteEl2));
    }
    
    @Test
    public void provaEliminarPeça99() throws UtilitatPersistenciaException{
        Municipi a=new Municipi();
        a.setId_municipi(99);
        aplicBD.eliminar(a);
        assertTrue(comparaLlistesMunicipis(aplicBD.obtenirMunicipis(),totsMunicipis));
    }
    
    @Test
    public void provaEliminarComarca5() throws UtilitatPersistenciaException{
        Comarca d=new Comarca();
        d.setId_comarca(5);
        aplicBD.eliminar(d);    // es la comarca que no te cap municipi associat; amb la resta dona error d'integritat
        assertTrue(comparaLlistesComarques(aplicBD.obtenirComarques(),totesComarquesExcepteEl5));
    }
    
    @Test  
    public void provaEliminarComarca99() throws UtilitatPersistenciaException{
        Comarca d=new Comarca();
        d.setId_comarca(99);
        aplicBD.eliminar(d);    
        assertTrue(comparaLlistesComarques(aplicBD.obtenirComarques(),totesComarques));
    }
    
    @Test
    public void provaModificarMunicipi33() throws UtilitatPersistenciaException{
        Municipi a=new Municipi();
        a.setId_municipi(33);
        a=aplicBD.refrescar(a);
        a.setNom("###"); a.setVegueria("###"); a.setDensitat(30);
        aplicBD.emmagatzemar(a);
        assertTrue(comparaLlistesMunicipis(aplicBD.obtenirMunicipis(),totesMunicipisCanviatEl3));
    }        
    
    @Test
    public void provaModificarMunicipi99() throws UtilitatPersistenciaException{
        Municipi a=new Municipi();
        a.setId_municipi(99);
        a=aplicBD.refrescar(a);
        if(a!=null){
              a.setNom("###"); a.setVegueria("###"); a.setDensitat(30);
            aplicBD.emmagatzemar(a);
        }
        assertTrue(comparaLlistesMunicipis(aplicBD.obtenirMunicipis(),totsMunicipis));
    }

    public void provaModificarComarca3() throws UtilitatPersistenciaException{
        Comarca d=new Comarca();
        d.setId_comarca(3);
        d=aplicBD.refrescar(d);
        d.setNom("###");  d.setCapital("@@@@@");  d.setSuperficie(200);
        aplicBD.emmagatzemar(d);
        assertTrue(comparaLlistesComarques(aplicBD.obtenirComarques(),totesComarquesCanviatEl3));
    }

    @Test
    public void provaModificarPrototip99() throws UtilitatPersistenciaException{
        Comarca d=new Comarca();
        d.setId_comarca(99);
        d=aplicBD.refrescar(d);
        if(d!=null){
            d.setNom("###");  d.setCapital("@@@@@");  d.setSuperficie(200);
            aplicBD.emmagatzemar(d);
        }
        assertTrue(comparaLlistesComarques(aplicBD.obtenirComarques(),totesComarques));
    }
    
    @Test
    public void provaConsultarMunicipi55() throws UtilitatPersistenciaException{
        Municipi a=new Municipi();
        a.setId_municipi(55);
        a=aplicBD.refrescar(a);
        assertTrue(comparaMunicipis(a,totsMunicipis.get(4)));
    }

    @Test
    public void provaConsultaMunicipi99() throws UtilitatPersistenciaException{
        Municipi a=new Municipi();
        a.setId_municipi(99);
        a=aplicBD.refrescar(a);
        assertNull(a);
    }

    @Test
    public void provaConsultarComarca3() throws UtilitatPersistenciaException{
        Comarca d=new Comarca();
        d.setId_comarca(3);
        d=aplicBD.refrescar(d);
        assertTrue(comparaComarques(d,totesComarques.get(2)));
    }

    @Test
    public void provaConsultarComarca99() throws UtilitatPersistenciaException{
        Comarca d=new Comarca();
        d.setId_comarca(99);
        assertNull(aplicBD.refrescar(d));
    }     
    
    
    /**
     * Dona d'alta tots els objectes del vector <code>totesAltes</code>
     * @throws UtilitatPersistenciaException en cas d'error
     */
    
    private void fesAltes() throws UtilitatPersistenciaException{        
        for(Object o:totesAltes){
            if(o instanceof Comarca){
                aplicBD.emmagatzemar((Comarca) o);
            }
            if(o instanceof Municipi){
                aplicBD.emmagatzemar((Municipi) o);
            }
        }
    }
    
    /**
     * Crea un municipi amb les dades indicades pels parametres
     * @param id codi que identifica el nou municipi
     * @param nom nom del nou municipi
     * @param vegueria vegueria al al que perteny el nou municipi
     * @param desnitat desnitat de població que té del nou municipi
     * @param contrada comarca al que pertany del nou municipi
     * @return municipi creat
     */
    
    private Municipi creaMunicipi(int id, String nom, String vegueria, float densitat, Comarca contrada){
        
        Municipi comu = new Municipi();
        
        comu.setId_municipi(id);
        comu.setNom(nom);
        comu.setVegueria(vegueria);
        comu.setDensitat(densitat);
        comu.setComarca(contrada);
        
        return comu;
    }
    
    /**
     * Crea una comarca amb les dades indicades pels parametres i sense municipis
     * assignats
     * @param codi codi que identifica la nova comarca
     * @param nom nom de la nova comarca
     * @param capital fabricant de la nova comarca
     * @param km2 superfície de la nova comarca
     * @return comarca creada
     */
    
    private  Comarca creaComarca(int codi, String nom, String capital, int km2){
        
        Comarca contrada = new Comarca();
        
        contrada.setId_comarca(codi);
        contrada.setNom(nom);
        contrada.setCapital(capital);
        contrada.setSuperficie(km2);
        
        return contrada;
    }
    
    /**
     * crea comarques i municipis i els emmagatzema a les llistes on aniran els
     * resultats esperats de les diferents proves
     */
    
    private  void creaDadesPrincipals(){  // s'omplen les llistes amb les dades de prova i els resultats de les consultes
        
        
        totesComarques.add(creaComarca(1,"Alt Camp","Valls", 545));      
        totesComarques.add(creaComarca(2,"Vallès Occidental","Sabadell i Terrassa", 545));
	totesComarques.add(creaComarca(3,"Baix Empordà","Empúries", 701));
	totesComarques.add(creaComarca(4,"Pallars Jussà","Tremp", 1343));
	totesComarques.add(creaComarca(5,"Baix Ebre","Tortosa", 1002));


        totsMunicipis.add(creaMunicipi(11,"Begur","Comarques gironines", 189.61F, totesComarques.get(2)));				totesComarques.get(2).getMunicipis().add(totsMunicipis.get(0));
        totsMunicipis.add(creaMunicipi(22,"Alcover","Camp de Tarragona", 110.09F, totesComarques.get(0)));				totesComarques.get(0).getMunicipis().add(totsMunicipis.get(1));
        totsMunicipis.add(creaMunicipi(33,"Palafrugell","Comarques gironines", 854.05F, totesComarques.get(2)));			totesComarques.get(2).getMunicipis().add(totsMunicipis.get(2));
        totsMunicipis.add(creaMunicipi(44,"Cerdanyola del Vallès","Àmbit Metropolità de Barcelona", 1875.92F, totesComarques.get(1)));	totesComarques.get(1).getMunicipis().add(totsMunicipis.get(3));
        totsMunicipis.add(creaMunicipi(55,"Puigpelat","Camp de Tarragona", 189.61F, totesComarques.get(0)));				totesComarques.get(0).getMunicipis().add(totsMunicipis.get(4));
        totsMunicipis.add(creaMunicipi(66,"La Pobla de Segur", "Alt Pirineu i Aran", 90.98F, totesComarques.get(3)));			totesComarques.get(3).getMunicipis().add(totsMunicipis.get(5));
        totsMunicipis.add(creaMunicipi(77,"Palamós","Comarques gironines", 1279.29F, totesComarques.get(2)));				totesComarques.get(2).getMunicipis().add(totsMunicipis.get(6));
        totsMunicipis.add(creaMunicipi(88,"Llimiana","Alt Pirineu i Aran", 3.48F, totesComarques.get(3)));				totesComarques.get(3).getMunicipis().add(totsMunicipis.get(7));

        
        totesAltes.addAll(totesComarques);
        totesAltes.addAll(totsMunicipis);
        
        for(Comarca d:totesComarques){
            if(d.getId_comarca()!=5){
                totesComarquesExcepteEl5.add(copia(d));
            }
            Comarca aux=copia(d);
            if(aux.getId_comarca()==3){
                aux.setNom("###");  aux.setCapital("@@@"); aux.setSuperficie(3000);
            }
            totesComarquesCanviatEl3.add(aux);
            
            if(d.getSuperficie()==1002){
                comarquesDeSuperficie1002.add(copia(d));
            }else if(d.getSuperficie()==545){
                comarquesDeSuperficie545.add(copia(d));
            }
            
        }
        
        for(Municipi a:totsMunicipis){
            if(a.getId_municipi()!=22){
                totesMunicipisExcepteEl2.add(copia(a));
            }
            Municipi aux=copia(a);
            if(aux.getId_municipi()==33){
                aux.setNom("###"); aux.setNom("###"); aux.setVegueria("###"); aux.setDensitat(30);
                
            }
            
            if(a.getComarca().getCapital().equals("Sabadell i Terrassa")){
                municipisDeSabadellTerrassa.add(copia(a));
            }else if(a.getComarca().getCapital().equals("Valls")){
                municipisDeValls.add(copia(a));
            }
            
            if(189.61==a.getDensitat()){
                municipisDe189.add(copia(a));
            }else if(a.getDensitat()==110.09){
                municipisDe110.add(copia(a));
            }
            
            totesMunicipisCanviatEl3.add(aux);
        } // fi del for que recorre tots els municipis
        
    }
    
    /**
     * Compara llistes dues llistes de peces sense tenir en compte l'ordre
     * @param a primera llista a comparar
     * @param b segona llista a comparar
     * @return cert si totes dues llistes son iguals i fals en cas contrari
     */
    
    private boolean comparaLlistesMunicipis(List <Municipi> a, List<Municipi> b){
        if(a.size()!=b.size()){
            return false;
        }
        for(Municipi e:a){
            if(!conteMunicipi(b,e)){
                return false;
            }
        }
        return true;
    }
    
    /**
     * Comprova si el municipi <code>em</code> es a la llista <code>l</code>
     * @param l llista de municipis on mirem si es troba el municipi
     * @param em un municipi que cerquem a la llista
     * @return cert si el municipi al llistat, fals en cas contrari
     */
    
    private boolean conteMunicipi(List<Municipi> l, Municipi em){
        for(Municipi e:l){
            if(comparaMunicipis(em,e)){
                return true;
            }
        }
        return false;
    }
    
    
    /**
     * Comprova si la comarca <code>et</code> es al llistat <code>l</code>
     * @param l  llistat on cerquem la comarca
     * @param et comarca que cerquem al llistat
     * @return cert si es troba una comarca igual a <code>et</code> a la llista
     * <code>l</code>. Fals en cas contrari.
     */
    
    private boolean conteComarca(List<Comarca> l, Comarca et){
        for(Comarca e:l){
            if(comparaComarques(et,e)){
                return true;
            }
        }
        return false;
    }    
    
    /**
     * obre la connexio amb la base de dades
     */
    
    private void obre(){
        try{
        
        aplicBD.iniciar();
        aplicBD.obrir();

        }catch(UtilitatPersistenciaException e){
            System.out.println("@@@@@@@@@");
            System.out.println(e.getMessage());
            System.out.println("@@@@@@@@@");
        }
    }
    
    /**
     * tanca la connexio amb la base de dades
     */
    
    private void tanca(){
        aplicBD.tancar();
    }
    
    // diferents utilitats per comparar llistes i objectes i copiar objectes
    
    private boolean comparaLlistesComarques(List <Comarca> a, List<Comarca> b){
        
        if(a.size()!=b.size()){
            return false;
        }
        
        for(Comarca e:a){
            if(!conteComarca(b,e)){
                return false;
            }
        }
        
        return true;
    }

    
    private boolean comparaStrings(String s1, String s2){
        if(s1==s2){
            return true;
        }
        
        if(s1==null){
            return false; //si s2 valgues null ja no hauriem arribat aqui
        }
        
        return s1.equals(s2);
    }
    
    private boolean comparaComarques(Comarca d1, Comarca d2){
        if(d1==d2){
            return true;
        }
        
        if(d1==null || d2==null){
            return false; //un es null i l'altre no
        }
        /*
        System.out.println("mun1: "+d1.getMunicipis());
        System.out.println("mun2: "+d2.getMunicipis());
        return false; 
        */
        
        return 
        d1.getId_comarca()==d2.getId_comarca() 
        && comparaStrings(d1.getNom(),d2.getNom()) 
        && comparaStrings(d1.getCapital(),d2.getCapital()) 
        && d1.getSuperficie()==d2.getSuperficie()
        && comparaLlistesMunicipis(d1.getMunicipis(), d2.getMunicipis());
        
    }
    
    private boolean comparaMunicipis(Municipi e1, Municipi e2){
        if(e1==e2){
            return true;
        }
        
        if(e1==null || e2==null){
            return false; //un es null i l'altre no
        }        
        
        if(e1.getId_municipi()!=e2.getId_municipi()|| !comparaStrings(e1.getNom(),e2.getNom())||!comparaStrings(e1.getVegueria(),e2.getVegueria())||e1.getDensitat()!=e2.getDensitat()){
            return false;
        }
        
        // tots dos son de la mateixa classe
        

        return comparaComarcaPla(e1.getComarca(),e2.getComarca());
 
    }

    private boolean comparaComarcaPla(Comarca d1, Comarca d2) {
        return d1.getId_comarca()==d2.getId_comarca() && comparaStrings(d1.getNom(),d2.getNom()) && comparaStrings(d1.getCapital(),d2.getCapital()) && d1.getSuperficie()==d2.getSuperficie();
    }
    
    private Comarca copia(Comarca d) {
        if(d==null){
            return null;
        }
        Comarca resultat=new Comarca();
        
        resultat.setId_comarca(d.getId_comarca());
        resultat.setNom(d.getNom());
        resultat.setCapital(d.getCapital());
        resultat.setSuperficie(d.getSuperficie());
        
        
        for(Municipi aux:d.getMunicipis()){
            resultat.getMunicipis().add(copia(aux,resultat));
        }
        return resultat;
    }

    private Municipi copia(Municipi a) {
        if(a==null){
            return null;
        }
        Comarca d=null;

        d=a.getComarca();

        return copia(a,copia(d));
    }
    
    private Municipi copia(Municipi a, Comarca d) {
        if(a==null){
            return null;
        }        

        Municipi resultat=new Municipi();
        resultat.setId_municipi(a.getId_municipi());
        resultat.setNom(a.getNom());
        resultat.setVegueria(a.getVegueria());
        resultat.setDensitat(a.getDensitat());
        resultat.setComarca(d);

        return resultat;
    }
    
    
}
