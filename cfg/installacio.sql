-- Table: Company
-- DROP TABLE Company;
CREATE TABLE comarca (
	  id_comarca INTEGER NOT NULL,
	  nom VARCHAR(40) NOT NULL,
	  capital VARCHAR(40),
	  superficie INTEGER,
	  CONSTRAINT comarca_pkey PRIMARY KEY (id_comarca)
	)
	WITH (
  		OIDS=FALSE
	);


ALTER TABLE comarca
	OWNER TO ioc;

CREATE TABLE municipi (
	  id_municipi INTEGER NOT NULL,
	  nom VARCHAR(50) NOT NULL,
	  vegueria VARCHAR(50) NOT NULL,
	  densitat FLOAT,
	  integrat INTEGER,
	  CONSTRAINT municipi_pkey PRIMARY KEY (id_municipi),
	  CONSTRAINT municipi_comarca_fkey FOREIGN KEY (integrat) REFERENCES comarca(id_comarca)
	)
	WITH (
  		OIDS=FALSE
	);

ALTER TABLE municipi
	OWNER TO ioc;

