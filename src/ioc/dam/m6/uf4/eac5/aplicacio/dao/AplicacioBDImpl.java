/** To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.dam.m6.uf4.eac5.aplicacio.dao;

import ioc.dam.m6.uf4.eac5.aplicacio.Comarca;
import ioc.dam.m6.uf4.eac5.aplicacio.Municipi;
import ioc.dam.m6.persistencia.excepcions.UtilitatPersistenciaException;
import java.util.List;
import ioc.dam.m6.uf4.eac5.aplicacio.AplicacioBD;

/**
 *
 * @author Professor
 * 
 */
public class AplicacioBDImpl extends AplicacioBDJdbc implements AplicacioBD {

    ComarcaDao comarcaDao;
    
    MunicipiDao municipiDao;
 
    
    @Override
    public void obrir() throws UtilitatPersistenciaException {
        super.obrir();
        comarcaDao = new ComarcaDao(con);
 
        municipiDao = new MunicipiDao(con);
     
    }
    
    @Override
    public void emmagatzemar(Comarca contrada) 
                        throws UtilitatPersistenciaException {
        comarcaDao.emmagatzemar(contrada);
    }

    @Override
    public void emmagatzemar(Municipi comu) throws UtilitatPersistenciaException {
        municipiDao.emmagatzemar(comu);
    }


    @Override
    public void eliminar(Comarca contrada) throws UtilitatPersistenciaException {
        comarcaDao.eliminar(contrada);
    }

    @Override
    public void eliminar(Municipi comu) throws UtilitatPersistenciaException {
        municipiDao.eliminar(comu);
    }


    @Override
    public Comarca refrescar(Comarca contrada) throws UtilitatPersistenciaException {
        return comarcaDao.refrescar(contrada);
    }

    @Override
    public Municipi refrescar(Municipi comu) throws UtilitatPersistenciaException {
        return municipiDao.refrescar(comu);
    }


    @Override
    public List<Comarca> obtenirComarques()  
                                throws UtilitatPersistenciaException {
        return comarcaDao.obtenirTot();
    }

    @Override
    public List<Municipi> obtenirMunicipis()  
                                throws UtilitatPersistenciaException {
        return municipiDao.obtenirTot();
    }


    @Override
    public List<Municipi> obtenirMunicipisPerDensitat(float habKm2) throws UtilitatPersistenciaException {
        return municipiDao.obtenirMunicipisPerDensitat(habKm2);
    }

    @Override
    public List<Municipi> obtenirMunicipisPerCapitalComarca(String urb) throws UtilitatPersistenciaException {
        return municipiDao.obtenirMunicipisPerCapitalComarca(urb);
    }

    @Override
    public List<Comarca> obtenirComarquesPerSuperficie(int km2) throws UtilitatPersistenciaException {
        return comarcaDao.obtenirComarquesPerSuperficie(km2);
    }


}
