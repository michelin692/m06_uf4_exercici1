/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.dam.m6.uf4.eac5.aplicacio.dao;

import ioc.dam.m6.uf4.eac5.aplicacio.Comarca;
import ioc.dam.m6.persistencia.AbstractJdbcDaoSimplificat;
import ioc.dam.m6.persistencia.JdbcPreparedDao;
import ioc.dam.m6.persistencia.JdbcPreparedQueryDao;
import ioc.dam.m6.persistencia.JdbcQueryDao;
import ioc.dam.m6.persistencia.UtilitatJdbcPlus;
import ioc.dam.m6.persistencia.excepcions.UtilitatJdbcSQLException;
import ioc.dam.m6.persistencia.excepcions.UtilitatPersistenciaException;
import ioc.dam.m6.uf4.eac5.aplicacio.Municipi;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Professor
 * 
 */
public class ComarcaDao extends AbstractJdbcDaoSimplificat<Comarca> {

    /**
     * Constructor que rep una connexio per parametre.
     * @param connection la connexio amb la que aquest objecte treballa
     */ 
    
    public ComarcaDao(Connection connection) {
        super(connection);
    }
    //TODO completar aquest metode
    @Override
    protected boolean esPersistent(final Comarca contrada) throws UtilitatPersistenciaException {
	boolean  esPersistent = false;
            
        JdbcPreparedQueryDao jdbcDao = new JdbcPreparedQueryDao(){
            @Override
            public Object writeObject(ResultSet rs) throws SQLException {
                return rs.getInt(1);
            }

            @Override
            public String getStatement() {
                return "select count(nom) from comarca where nom=?";
            }

            @Override
            public void setParameter(PreparedStatement pstm) throws SQLException {
                int field=0;
                pstm.setString(++field, contrada.getNom());
            }
        } ;
            
        esPersistent = ((Integer)UtilitatJdbcPlus.obtenirObjecte(con, jdbcDao))>=1;
        
        return esPersistent;
    }
    

    //TODO completar aquest metode
    @Override
    protected void modificar(final Comarca contrada) throws UtilitatPersistenciaException {
       JdbcPreparedDao jdbcPreparedDao = new JdbcPreparedDao() {

            @Override
            public void setParameter(PreparedStatement pstm) throws SQLException {
                int field=0;
                pstm.setString(++field, contrada.getNom());
                pstm.setString(++field, contrada.getCapital());
                pstm.setInt(++field, contrada.getSuperficie());
            }

            @Override
            public String getStatement() {
                return "update comarca set nom=?, capital=? , superficie=?  where nom=?";
            }
        };
        UtilitatJdbcPlus.executar(con, jdbcPreparedDao);
    }

    //TODO completar aquest metode
    @Override
    protected void inserir(final Comarca contrada) throws UtilitatPersistenciaException {
       JdbcPreparedDao jdbcPreparedDao = new JdbcPreparedDao() {
           @Override
            public void setParameter(PreparedStatement pstm) throws SQLException {
                int field=0;
                pstm.setInt(++field, contrada.getId_comarca());
                pstm.setString(++field, contrada.getNom());
                pstm.setString(++field, contrada.getCapital());
                pstm.setInt(++field, contrada.getSuperficie());
            }

            @Override
            public String getStatement() {
                return "insert into comarca(id_comarca, nom, capital, superficie) values(?,?,?,?)";
            }
        };
        UtilitatJdbcPlus.executar(con, jdbcPreparedDao);
    }
    

    //TODO completar aquest metode
    @Override
    public Comarca refrescar(final Comarca contrada) throws UtilitatPersistenciaException {
        Comarca regio;
        JdbcPreparedQueryDao jdbcConsultaDao = new JdbcPreparedQueryDao() {
            @Override
            public Object writeObject(ResultSet rlts) throws SQLException {
                int camp=0;
                //- omplint les dades propies de la comarca 
                contrada.setCapital(rlts.getString(++camp));
                contrada.setId_comarca(rlts.getInt(++camp));
                contrada.setNom(rlts.getString(++camp));
                contrada.setSuperficie(rlts.getInt(++camp));
                //bucle que avanci pel ResultSet, omplir la llista dels seus municipis
                if(contrada.getMunicipis().size() > 0 ){
                    contrada.getMunicipis().clear();
                } ;
                
                try {
                    MunicipiDao muni = new MunicipiDao(con);
                    List<Municipi> listdo_mun = muni.obtenirMunicipisPerCapitalComarca(contrada.getCapital());
                    contrada.municipis.addAll(listdo_mun);
                } catch (UtilitatPersistenciaException ex) {
                    //Logger.getLogger(ComarcaDao.class.getName()).log(Level.SEVERE, null, ex);
                    contrada.municipis.add(null);
                }
                
                return contrada;
                
            }
  
            @Override
            public String getStatement() {
                // cal posar la instruccio SQL necessaria per obtenir un recordset amb les dades
                // de la comarca amb els seus municipis
               return " select capital ,id_comarca , nom ,superficie from comarca where id_comarca = ? ";
                /*
                return " select "
                       + " comarca.capital"
                       + " ,comarca.id_comarca"
                       + " ,comarca.nom"
                       + " ,comarca.superficie"
                       + " ,municipi.id_municipi"
                       + " ,municipi.nom"
                       + " ,municipi.densitat"
                       + " ,municipi.vegueria"
                       + " from comarca"
                       + " inner join municipi"
                       + " on "
                       + " municipi.integrat = comarca.id_comarca "
                       + " where comarca.id_comarca = ?";
                */
            }

            @Override
            public void setParameter(PreparedStatement prdStm) throws SQLException {
                // tambe cal omplir els parametres de la instruccio SQL retornada a getStatement()
                int camp=0;
                prdStm.setInt(++camp,contrada.getId_comarca()) ;
            };
        };
        regio = (Comarca) UtilitatJdbcPlus.obtenirObjecte(con, jdbcConsultaDao);
        
        return regio;
    }

    //TODO completar aquest metode
    @Override
    public void eliminar(final Comarca contrada) throws UtilitatPersistenciaException {
        JdbcPreparedDao jdbcDao = new JdbcPreparedDao() {
            @Override
            public void setParameter(PreparedStatement pstm) throws SQLException {
                int field=0;
                pstm.setInt(++field, contrada.getId_comarca());
            }

            @Override
            public String getStatement() {
                return "delete from comarca where id_comarca = ?";
            }
        };
        UtilitatJdbcPlus.executar(con, jdbcDao);
    }

    //TODO completar aquest metode
    @Override
    public List<Comarca> obtenirTot() throws UtilitatPersistenciaException {
        
        JdbcQueryDao jdbcDao = new JdbcQueryDao() {
            
            @Override
            public Object writeObject(ResultSet rs) throws SQLException {
                int field=0;
                
                Comarca comarca = new Comarca();
                comarca.setId_comarca(rs.getInt(++field));
                comarca.setNom(rs.getString(++field));
                comarca.setCapital(rs.getString(++field));
                comarca.setSuperficie(rs.getInt(++field));
                
                try {
                    MunicipiDao muni = new MunicipiDao(con);
                    List<Municipi> listdo_mun = muni.obtenirMunicipisPerCapitalComarca(comarca.getCapital());
                    comarca.municipis.addAll(listdo_mun);
                } catch (UtilitatPersistenciaException ex) {
                    Logger.getLogger(ComarcaDao.class.getName()).log(Level.SEVERE, null, ex);
                }
               
                return comarca;
            }

            @Override
            public String getStatement() {
                return "SELECT id_comarca, nom,  capital , superficie  FROM comarca";
                /*return " SELECT "
                         + " contrada.id_comarca"
                         + " ,contrada.nom"
                         + " ,contrada.capital"
                         + " ,contrada.superficie"
                         + " ,comu.id_municipi"
                         + " ,comu.nom"
                         + " ,comu.vegueria"
                         + " ,comu.densitat"
                         + " FROM comarca contrada "
                         + " LEFT JOIN municipi comu "
                         + " ON comu.integrat = contrada.id_comarca";
                */
            }

        };
        List<Comarca> comarcas = UtilitatJdbcPlus.obtenirLlista(con, jdbcDao);
        return comarcas;
        
    }

    /**
     * Permet obtenir una llista de totes les comarques del sistema de
     * persistencia de l'extensió indicada pel parametre.
     * No inclou les dades dels municipis que aquestes comarques tenen assignades.
     * Per obtenir-les, cal refrescar cada comarca.
     * @param extensio superfície em Km^2 que han fet que les comarques formen part del resultat.
     * @return llista amb totes les comarques del sistema de persistencia de
     * de la superfície indicada pel parametre
     * @throws UtilitatJdbcSQLException si es produeix un error 
     */   
    //TODO completar aquest metode
    public List<Comarca> obtenirComarquesPerSuperficie(int extensio) throws UtilitatJdbcSQLException {
	return null;   //instruccio que cal canviar; nomes hi es perque es pugui compilar    
    }

}
