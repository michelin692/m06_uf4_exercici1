/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.dam.m6.uf4.eac5.aplicacio;

import ioc.dam.m6.persistencia.excepcions.UtilitatPersistenciaException;
import java.util.List;

/**
 *
 * @author Professor
 */
public interface AplicacioBD {
    /**
     * Obte una connexio a partir de les dades configurades en invocar el 
     * metode <code>iniciar</code>. Un cop obtinguda la connexio, aquesta 
     * romandra oberta i es podra reciclar fins que no s'invoqui el metode 
     * <code>tancar</code>. Abans d'invocar aquest metode, cal haver invocat 
     * el metode <code>iniciar</code>. En cas que s'invoqui obrir sense cap 
     * invocacio previa del metode <code>iniciar</code> produira un error que es 
     * materialitzara en el llancament de <code>UtilitatPersistenciaException</code>.
     * El metode iniciar nomes s'ha d'invocar una unica vegada. Un cop invocat, 
     * si les dades de configuracio son correctes i el SGBD es accessible, 
     * no hauria de produir-se cap error.
     * @throws UtilitatPersistenciaException 
     */    
    void obrir() throws UtilitatPersistenciaException;

    /**
     * Tanca la connexio oberta des de la invocacio del metode <i>obrir</i>. 
     * La base de dades ha d'estar aixecada i ha de mantenir oberta la connexio.
     * En cas d'error, aquest sera enregistrat en un fitxer, pero no es 
     * llancara cap excepcio per evitar una excessiva imbrincacio de sentencies 
     * try-catch.
     */    
    void tancar();
    

    /**
     * Prepara la base de dades per treballar amb ell; 
     * cal invocar-lo nomes un cop i abans de qualsevol 
     * de les altres operacions.
     * @throws UtilitatPersistenciaException si es produeix un error
     */
    void iniciar() throws UtilitatPersistenciaException;
    
    /**
     * Emmagatzema una comarca a la base de dades. Si ja hi existeix
     * un altre amb la mateixa clau, l'actualitza amb les dades del parametre.
     * No actualitza els estudiants que en fan ús.
     * @param contrada
     * @throws UtilitatPersistenciaException si es produeix un error 
     */
    void emmagatzemar(final Comarca contrada) throws UtilitatPersistenciaException ;

    /**
     * Emmagatzema un municipi a la base de dades. Si ja hi existeix
     * una altre amb la mateixa clau, l'actualitza amb les dades del parametre.
     * Actualitza el prototip que te assignat, pero no les dades d'aquesta.
     * @param comu
     * @throws UtilitatPersistenciaException si es produeix un error 
     */
    void emmagatzemar(final Municipi comu) throws UtilitatPersistenciaException ;

    /**
     * Elimina una comarca de la base de dades. El localitza per la
     * clau. Si no hi existeix cap amb la mateixa clau, no fa res. Si té assignats
     * peces, es produeix un error i no es realitza l'eliminacio
     * @param contrada
     * @throws UtilitatPersistenciaException si es produeix un error 
     */
    void eliminar(final Comarca contrada) throws UtilitatPersistenciaException ;
    
    /**
     * Elimina un municipi de la base de dades. El localitza per la clau.
     * Si no hi existeix cap amb la mateixa clau, no fa res.
     * @param comu
     * @throws UtilitatPersistenciaException  si es produeix un error
     */
    void eliminar(final Municipi comu) throws UtilitatPersistenciaException ;

    /**
     * Actualitza les dades a memoria d'una comarca amb les que te a la base de
     * dades. El localitza per la clau. Si la base de dades no conte cap 
     * comarca amb la mateixa clau, retorna null i no modifica el parametre. 
     * Si el troba, pero, actualitza tambe la llista de municipis
     * @param contrada comarca que volem actualitzar
     * @return referencia al prototip actualitzat; null si no existeix a la base de dades
     * @throws UtilitatPersistenciaException si es produeix un error 
     */
    Comarca refrescar(final Comarca contrada) throws UtilitatPersistenciaException ;
    
    /**
     * Actualitza les dades a memoria d'un municipi amb les que hi ha a la base de
     * dades. El localitza per la clau. Si la base de dades no conte cap municipi 
     * amb la mateixa clau, retorna null i no modifica el parametre. Si el troba,
     * però, actualitza tambe les dades de la comarca assignada.
     * @param comu municipi que volem actualitzar
     * @return referencia al municipi actualitzat; null si no existeix a la
     * base de dades.
     * @throws UtilitatPersistenciaException  si es produeix un error
     */
    Municipi refrescar(final Municipi comu) throws UtilitatPersistenciaException ;

    /**
     * Permet obtenir un llistat amb totes les comarques de la base de dades.
     * Inclou les dades dels municipis que té la comarca.
     * @return llista amb totes les comarques de la base de dades
     * @throws UtilitatPersistenciaException si es produeix un error 
     */
    List<Comarca> obtenirComarques() throws UtilitatPersistenciaException ;
    
    /**
     * Permet obtenir una llista de tots els municipis de la base de dades.
     * Inclou les dades de la seva comarca.
     * @return llistat amb tots els municipis de la base de dades
     * @throws UtilitatPersistenciaException si es produeix un error 
     */
    List<Municipi> obtenirMunicipis() throws UtilitatPersistenciaException ;
    

    
    /**
     * Permet obtenir un llistat amb tots els municipis de la base de dades
     * amb una determinada desnitat de població. Inclou les dades de la seva comarca.
     * @param habKm2 desnitat de població dels municipis que cal recuperar
     * @return llistat amb totes els municipis amb de la base de dades amb la
     * desnitat de població indicada pel parametre.
     * @throws UtilitatPersistenciaException si es produeix un error 
     */    
    List<Municipi> obtenirMunicipisPerDensitat(final float habKm2) 
                        throws UtilitatPersistenciaException ;  

    /**
     * Permet obtenir una llista de tots els municipis de la base de dades
     * que petanyen a una comarca amb una determinada capital. Inclou les dades de la seva comarca.
     * @param urb on és la capital de la comarca dels municipis que cal recuperar
     * @return llista amb tots els municipis de la base de dades de la comarca amb la capital
     * indicada pel parametre.
     * @throws UtilitatPersistenciaException si es produeix un error 
     */    
    public List<Municipi> obtenirMunicipisPerCapitalComarca(String urb) throws UtilitatPersistenciaException;

    /**
     * Permet obtenir un llistat de totes les comarques de la base de dades
     * de la superfície indicada pel parametre.
     * Inclou les dades dels municipis que té la comarca.
     * @param extensio superfície de les comarques que es cerquen.
     * @return llistat amb totes les comarques de la base de dades amb una superfície
     * indicada pel parametre
     * @throws UtilitatPersistenciaException si es produeix un error 
     */   
    List<Comarca> obtenirComarquesPerSuperficie(final int extensio) 
                        throws UtilitatPersistenciaException ;

}
