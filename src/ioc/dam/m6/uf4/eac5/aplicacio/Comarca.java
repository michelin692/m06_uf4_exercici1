/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.dam.m6.uf4.eac5.aplicacio;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe que representa una comarca de Catalunya
 * @author Professor
 */
public class Comarca {
    private int id_comarca;
    private String nom;
    private String capital;
    private int superficie;
    public final List<Municipi> municipis = new ArrayList();

    /**
     * Permet consultar l'identificador de comarca
     * @return id_comarca que identifica la comarca
     */    
    public int getId_comarca() {
        return id_comarca;
    }

    /**
     * Permet actualitzar l'identificador de comarca
     * @param codi valor a assignar a l'id_comarca que identifica la comarca
     */
    public void setId_comarca(int codi) {
        this.id_comarca = codi;
    }

    /**
     * Permet consultar el nom de la comarca
     * @return  nom de la comarca
     */
    public String getNom() {
        return nom;
    }

    /**
     * Permet actualitzar el nom de la comarca
     * @param toponim nom que s'assigna a la comarca
     */
    public void setNom(String toponim) {
        this.nom = toponim;
    }

    /**
     * Permet consultar la capital de la comarca
     * @return capital de la comarca
     */
    public String getCapital() {
        return capital;
    }

    /**
     * Permet modificar la capital de la comarca
     * @param poblacio capital a assignar a la comarca
     */
    public void setCapital(String poblacio) {
        this.capital = poblacio;
    }
    
    /**
     * Permet consultar la superfície en Km^2 de la comarca
     * @return  superfície en Km^2 de la comarca
     */
    public int getSuperficie() {
        return superficie;
    }

    /**
     * Permet actualitzar  la superfície en Km^2 de la comarca
     * @param km2 superfície en Km^2 que s'assigna a la comarca
     */
    public void setSuperficie(int km2) {
        this.superficie = km2;
    }
    
    /**
     * Permet consultar la referència a la llista amb tots els municipis
     * pertanyents a la comarca
     * @return llista amb els municipis pertanyents a la comarca
     */
    public List<Municipi> getMunicipis() {
        return (List<Municipi>) municipis;
    }
    
}
