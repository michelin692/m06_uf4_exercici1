/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.dam.m6.uf4.eac5.aplicacio;


/**
 * Classe que representa un municipi que forma part d'una comarca de Catalunya
 * @author Professor
 */
public class Municipi  {

    private int id_municipi;
    private String nom;
    private String vegueria;
    private float densitat;
    private Comarca integrat;


    /**
     * Permet consultar l'identificador d'un municipi
     * @return id_municipi que identifica el municipi
     */    
    public int getId_municipi() {
        return id_municipi;
    }

    /**
     * Permet actualitzar l'identificador d'un municipi
     * @param referencia nova referència que identifica el municipi
     */
    public void setId_municipi(int referencia) {
        this.id_municipi = referencia;
    }    
    
    /**
     * Permet consultar el nom que designa el municipi
     * @return nom del municipi
     */
    public String getNom() {
        return nom;
    }
    
    /**
     * Permet actualitzar el nom que designa el municipi
     * @param nom nom a assignar al municipi
     */
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    /**
     * Permet consultar la vegueria a la qual pertany el municipi
     * @return vegueria a la qual pertany el municipi
     */
    public String getVegueria() {
        return vegueria;
    }
    
    /**
     * Permet actualitzar la vegueria a la qual pertany el municipi
     * @param toponim vegueria a assignar al municipi
     */
    public void setVegueria(String toponim) {
        this.vegueria = toponim;
    }

    /**
     * Permet consultar la densitat en hab/km² del municipi
     * @return densitat en hab/km² del municipi
     */
    public float getDensitat() {
        return densitat;
    }
    
    /**
     * Permet actualitzar la densitat en hab/km² del municipi
     * @param habKm2 nova densitat en hab/km² del municipi
     */
    public void setDensitat(float habKm2) {
        this.densitat = habKm2;
    }
    
    /**
     * Permet consultar la comarca a la qual pertany el municipi
     * @return integrat comarca a la qual pertany el municipi
     */
    public Comarca getComarca() {
        return integrat;
    }

    /**
     * Permet actualitzar la comarca a la qual pertany el municipi
     * @param contrada nova comarca a la qual pertany el municipi
     */
    public void setComarca(Comarca contrada) {
        this.integrat = (Comarca)contrada;
    }
    
}
